# NodeJS

##Notes App
###Topics

####Modules
- [nodemon](https://nodemon.io/)
  - Moniters changes in source code and automatically restarts

- [yaml](http://yaml.org/)
  - Used to read and format command line arguments
  - Data serialization for all languages

- [lodash](https://lodash.com/)
  - Iterating over objects and arrays
  - Manipulating and testing values
  - Creating composite function


####Most Useful Info
- Debugging using inspect
  - [node/nodemon] inspect app.js
  - n: next
  - c: continue to next breakpoint
  - Add breakpoint; to code
  - repl used to inspect environment
