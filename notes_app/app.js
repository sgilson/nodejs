const fs = require('fs');
const os = require('os');
const notes = require('./notes.js');
const _ = require('lodash');
const yargs = require('yargs');

const cmdTitle = {
  describe: 'Title of note',
  demand: true,
  alias: 't'
};

const cmdBody = {
  describe: 'Content of note',
  demand: true,
  alias: 'b'
};

const argv = yargs
  .command('add', 'Add a new note.', {
    title: cmdTitle,
    body: cmdBody
  })
  .command('list', 'List all notes')
  .command('read', 'Read a note', {
    title: cmdTitle,
    body: cmdBody
  })
  .command('remove', 'Removes a note', {
    title: cmdTitle
  })
  .argv;
var command = argv._[0];
/*console.log('Command', command);
console.log('Yargs', argv);*/
switch(command){
  case 'add':
    let note = notes.addNote(argv.title, argv.body);
    if(note) {
      console.log("Note created:", argv.title);
      notes.logNote(note);
    }else {
      console.log("Note title taken.");
    }
    break;
  case 'remove':
    let noteRemoved = notes.removeNote(argv.title);
    var message = noteRemoved ? 'Note was removed.' : 'Note not found.';
    console.log(message);
    break;
  case 'list':
    let list = notes.getAll();
    console.log(`Printing ${list.length} notes.`);
    list.forEach((note) => notes.logNote(note));
    break;
  case 'read':
    let foundNote = notes.getNote(argv.title);
    if(foundNote) {
      notes.logNote(foundNote);
    } else {
      console.log("Note not found.");
    }
    break;
  default:
    console.log('Command not found.');
    break;
}
