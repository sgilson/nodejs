const fs = require('fs');

var fetchNotes = () => {
  try{
    return JSON.parse(fs.readFileSync('note-data.json'));
  }catch(e){
    return [];
  }
};

var saveNotes = (notes) => {
  fs.writeFileSync('note-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
  let notes = fetchNotes();
  let note = {title,body};
  let duplicateNotes = notes.filter((note) => note.title === title);
  if (duplicateNotes.length === 0) {
    notes.push(note);
    saveNotes(notes);
    return note;
  }
};

var getAll = () => {
  return fetchNotes();
};

var getNote = (title) => {
  let notes = fetchNotes();
  let filteredNotes = notes.filter((note) => note.title === title);
  return filteredNotes[0];
};

var removeNote = (title) => {
  let notes = fetchNotes();
  let filteredNotes = notes.filter((note) => note.title != title);
  saveNotes(notes);
  if (notes.length != filteredNotes.length){
    return true;
  }
  return false;
};

var logNote = (note) => {
  console.log('-----');
  console.log(`Title: ${note.title}`);
  console.log(`Body: ${note.body}`);
};

module.exports = {
  addNote,
  getAll,
  getNote,
  removeNote,
  logNote
}
