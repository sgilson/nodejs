const express = require('express');
const hbs = require('hbs');
const PORT = 3000;
var app = express();

app.set('view engine', 'hbs');
app.use(express.static(__dirname+'/public'));

app.get('/', (req, res) => {
  res.send('bitch');
});

app.get('/about', (req, res) => {
  res.render('about.hbs', {
    pageTitle: 'About page',
    year: new Date().getFullYear()
  });
});

app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unable to handle'
  })
})

app.listen(PORT, () => {
  console.log(`Server is up on port ${PORT}.`);
});
