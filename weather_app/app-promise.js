const yargs = require('yargs');
const axios = require('axios');

const KEY = '8171b165ab0e7732b22b54cd4f8fbbab';

const argv = yargs
  .options({
    a: {
      demand: true,
      alias:'address',
      describe:'Address to fetch weather for.',
      string:true
    }
})
  .help()
  .alias('help', 'h')
  .argv

var encodedAddress = encodeURIComponent(argv.address);
var geocodeURL = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

axios.get(geocodeURL).then(response => {
  if(response.data.status === 'ZERO_RESULTS') {
    throw new Error('Unable to find that address.');
  }
  var lat = response.data.results[0].geometry.location.lat;
  var lng = response.data.results[0].geometry.location.lng;
  var weatherURL = `https://api.darksky.net/forecast/${KEY}/${lat},${lng}`;
  console.log(response.data.results[0].formatted_address);
  return axios.get(weatherURL);
})
.then(response => {
  var temp = response.data.currently.temperature;
  var aTemp = response.data.currently.apparentTemperature;
  console.log(`It's currently ${temp}. It feels like ${aTemp}.`);
})
.catch(e => {
  if (e.code === 'ENOTFOUND') {
    console.log('Unable to connect to API service.');
  } else {
    console.log(e.message);
  }
})
