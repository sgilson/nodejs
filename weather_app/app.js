const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const KEY = '9b8ebc5d3dcac3a99ffc6bfc55d420f6';

const argv = yargs
  .options({
    a: {
      demand: true,
      alias:'address',
      describe:'Address to fetch weather for.',
      string:true
    }
})
  .help()
  .alias('help', 'h')
  .argv

geocode.geocodeAddress(argv.address, (error, results) => {
  if(error) {
    console.log(error);
  } else {
    weather.getWeather(KEY, results.lat, results.lng, (error, weatherResults) => {
      if(error){
        console.log(error);
      } else {
        formatResponse(results.address, results.lat, results.lng, weatherResults.temp, weatherResults.aTemp);
      }
    });
  }
});

function formatResponse(addr, lat, lng, temp, aTemp) {
  console.log('------------');
  console.log(`Location: ${addr}`);
  console.log(`Latitude: ${lat}`);
  console.log(`Longitude: ${lng}`);
  console.log(`It is currently ${temp}, but it feels like ${aTemp}.`);
  console.log('------------');
}
