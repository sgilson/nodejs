const request = require('request');

var weather = (key, lng, lat, callback) => {
  request({
    url: `https://api.darksky.net/forecast/${key}/${lng},${lat}`,
    json: true
  }, (error, response, body) => {
    if(!error && response.statusCode === 200) {
      callback(undefined, {
        temp: body.currently.temperature,
        aTemp: body.currently.apparentTemperature
      });
    } else {
      callback('Unable to fetch weather.');
    }
  }
  )
;}

module.exports.getWeather = weather;
